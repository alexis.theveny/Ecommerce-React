const express = require('express')
const userRouter = require('./router/utilisateur')
const articleRouter = require('./router/articles')
const commandRouter = require('./router/command')
const cors = require('cors')
const db = require('./data/database')
const app = express()

app.use(cors())
app.use(express.static("./assets")) //fichiers statiques
app.use(express.json()) //body en json API Rest


app.get("/", (req, res) => {
    res.send("<h1>Bienvenue sur l'API ecommerce</h1>")
})

app.use('/user', userRouter)
app.use('/product', articleRouter)
app.use('/command', commandRouter)


app.listen(3000)

