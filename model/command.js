const db = require("../data/database");
const jwt = require('jsonwebtoken');


function getAllCommands() {
    return new Promise((resolve, rej) => {
        db.all("SELECT * FROM command", (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })
}
function getAllCommandsByUser(usermail) {
    return new Promise((resolve, rej) => {
        db.all("SELECT * FROM command WHERE user=?",usermail, (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })
}
function getCommand(id) {
    return new Promise((resolve, rej) => {
        db.get("SELECT * FROM command WHERE id=?", id, (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })
}
function getCommandLines(id) {
    return new Promise((resolve, rej) => {
        db.all("SELECT command.date, command.statut, ligne_commande.qte, articles.nom, articles.description, articles.prix FROM command INNER JOIN ligne_commande ON ligne_commande.id_command = command.id INNER JOIN articles ON articles.id = ligne_commande.id_article WHERE command.id=? ", id, (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })
}
function createCommand(date, statut, id_user) {
    return new Promise((resolve, rej) => {
        db.run("INSERT INTO command (date,statut,user) VALUES(?,?,?)", [date, statut, id_user], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Commande créée", id: this.lastID })
        })
    })
}
function createCommandLine(id_command, articleList) {
    return new Promise((resolve, rej) => {
        let rows = ""
        articleList.forEach(article => {
            rows = rows + "("+id_command+","+article.id+","+article.qte+"),"
        });
        rows = rows.slice(0, -1);
        db.run("INSERT INTO ligne_commande (id_command,id_article,qte) VALUES ?".replace('?', rows), function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Ligne(s) de commande créée(s)" })
        })
    })
}
function deleteCommand(id) {
    return new Promise((resolve, rej) => {
        db.run("DELETE FROM command WHERE id=?", id, function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Commande supprimé" })
        })
    })
}
function deleteCommandLine(id_command) {
    return new Promise((resolve, rej) => {
        db.run("DELETE FROM ligne_commande WHERE id_command=?", id_command, function (err, res) {
            if (err) rej(err)
            resolve({ mess: "Ligne(s) de commande supprimée(s)" })
        })
    })
}
function updateCommand(id_command, statut) {
    return new Promise((resolve, rej) => {
        db.run("UPDATE command SET statut=? WHERE id=?", [statut, id_command], (err, res) => {
            if (err) rej(err)
            resolve({ mess: "Commande mise à jour" })
        })
    })
}


module.exports = {
    getAllCommands,
    getCommand,
    createCommand,
    createCommandLine,
    deleteCommand,
    deleteCommandLine,
    updateCommand,
    getAllCommandsByUser,
    getCommandLines

}