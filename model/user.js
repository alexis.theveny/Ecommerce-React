const db = require("../data/database");
const jwt = require('jsonwebtoken');

function getAllUsers() {
    return new Promise((resolve, rej) => {
        db.all("SELECT * FROM user", (err, res) => {
            if (err) rej(err)
            resolve(res)
        })
    })
}
function connectUser(email, pass) {
    return new Promise((resolve, rej) => {
        db.get("SELECT * FROM user WHERE email=?", email, (err, res) => {
            if (err) rej(err)
            if (res && res.pass == pass) {
                const token = jwt.sign({ user: res.email, niveau: res.niveau }, 'ma super clé');
                resolve({ token, admin: res.niveau })
            }
            rej({ mess: "utilisateur ou mot de passe incorrect" })
        })
    })
}
function newUser(email, pass) {
    return new Promise((resolve, rej) => {
        db.run("INSERT INTO user (email,pass,niveau) VALUES(?,?,0)", [email, pass], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "utilisateur ajouté", id: this.lastID })
        })
    })
}
function deleteUser(id) {
    return new Promise((resolve, rej) => {
        db.run("DELETE FROM user WHERE id = ?", [id], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "utilisateur supprimé" })
        })
    })
}
function updateUser(id, niveau) {
    return new Promise((resolve, rej) => {
        db.run("UPDATE user SET niveau = ? WHERE id = ?", [niveau, id], function (err, res) {
            if (err) rej(err)
            resolve({ mess: "utilisateur modifié" })
        })
    })
}

module.exports = {
    connectUser,
    newUser,
    deleteUser,
    updateUser,
    getAllUsers
}