const express = require('express')
const checkToken = require('../middleware/checkToken')
const { connectUser, newUser, deleteUser, updateUser, getAllUsers } = require('../model/user')
const routeur = express.Router()
//Connect
routeur.post('/connexion', (req, res) => {
    connectUser(req.body.mail, req.body.pass).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})
//inscription
routeur.post('/inscription', (req, res) => {
    newUser(req.body.mail, req.body.pass).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})
//supprimer
routeur.delete('/delete/:id', checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        deleteUser(req.params.id).then(data => {
            res.json(data)
        }).catch(err => {
            res.status(500).json(err)
        })
    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})
//mettre à niveau
routeur.put('/update/:id', checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        updateUser(req.params.id, req.body.niveau).then(data => {
            res.json(data)
        }).catch(err => {
            res.status(500).json(err)
        })
    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})
//tous les utilisateurs
routeur.get('/', (req, res) => {
    getAllUsers().then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})

module.exports = routeur