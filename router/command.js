const { json } = require('express')
const express = require('express')
const checkToken = require('../middleware/checkToken')
const { getAllCommands, getCommand, createCommand, createCommandLine, deleteCommand, deleteCommandLine, updateCommand, getAllCommandsByUser, getCommandLines } = require('../model/command')
const { getIdUserFromMail } = require('../model/user')
const routeur = express.Router()

//get ALL
routeur.get('/', checkToken, (req, res) => {
    if (req.user.email) {
        getAllCommandsByUser(req.user.email).then(data => {
            res.json(data)
        }).catch(err => {
            res.status(500).json(err)
        })

    } else {
        getAllCommands().then(data => {
            res.json(data)
        }).catch(err => {
            res.status(500).json(err)
        })
    }
})
//get One
routeur.get('/:id', (req, res) => {
    getCommand(req.params.id).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})
//get commands line
routeur.get('/lines/:id', (req, res) => {
    getCommandLines(req.params.id).then(data => {
        res.json(data)
    }).catch(err => {
        res.status(500).json(err)
    })
})
//Add
routeur.post('/', checkToken, (req, res) => {
    if (req.user) {
        let currentDateTime = new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString()
        createCommand(currentDateTime, "validé", req.user.mail).then(data => {
            let articleList = req.body.articles
            createCommandLine(data.id, articleList).then(data => {
                res.json(data)
            }).catch(err => {
                res.status(500).json(err)
            });
        }).catch(err => {
            res.status(500).json(err)
        })

    } else {
        res.status(403).json({ mess: "Vous devez être connecté" })
    }
})
//Supprimer
routeur.delete('/:id', checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        deleteCommand(req.params.id).then(data => {
            deleteCommandLine(req.params.id).then(data => {
                res.json(data)
            }).catch(err => {
                res.status(500).json(err)
            });
        }).catch(err => {
            res.status(500).json(err)
        })

    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})
//Update status
routeur.put('/:id', checkToken, (req, res) => {
    if (req.user.niveau === 1) {
        updateCommand(req.params.id, req.body.statut).then(data => {
            res.json(data)
        }).catch(err => {
            res.status(500).json(err)
        })

    } else {
        res.status(403).json({ mess: "Vous devez être administrateur" })
    }
})

module.exports = routeur